# Original Project by ChenYingpeng

https://github.com/ChenYingpeng/darknet2caffe

# Requirements

  Python2.7

  Caffe

  Pytorch >= 0.40
# Add Caffe Layers
1. Copy `caffe_layers/mish_layer/mish_layer.hpp,caffe_layers/upsample_layer/upsample_layer.hpp` into `include/caffe/layers/`.
2. Copy `caffe_layers/mish_layer/mish_layer.cpp mish_layer.cu,caffe_layers/upsample_layer/upsample_layer.cpp upsample_layer.cu` into `src/caffe/layers/`.
3. Copy `caffe_layers/pooling_layer/pooling_layer.cpp` into `src/caffe/layers/`.Note:only work for yolov3-tiny,use with caution.
4. Add below code into `src/caffe/proto/caffe.proto`.

```
// LayerParameter next available layer-specific ID: 147 (last added: recurrent_param)
message LayerParameter {
  optional TileParameter tile_param = 138;
  optional VideoDataParameter video_data_param = 207;
  optional WindowDataParameter window_data_param = 129;
++optional UpsampleParameter upsample_param = 149; //added by chen for Yolov3, make sure this id 149 not the same as before.
++optional MishParameter mish_param = 150; //added by chen for yolov4,make sure this id 150 not the same as before.
}

// added by chen for YoloV3
++message UpsampleParameter{
++  optional int32 scale = 1 [default = 1];
++}

// Message that stores parameters used by MishLayer
++message MishParameter {
++  enum Engine {
++    DEFAULT = 0;
++    CAFFE = 1;
++    CUDNN = 2;
++  }
++  optional Engine engine = 2 [default = DEFAULT];
++}
```
5.remake caffe.

# Demo
  $ python cfg[in] weights[in] prototxt[out] caffemodel[out]

  Example
```
python darknet2caffe_nomish.py cfg/yolov4.cfg weights/yolov4.weights caffe_yolov4/yolov4.prototxt caffe_yolov4/yolov4.caffemodel
```
  partial log as below.
```
I0522 10:19:19.015708 25251 net.cpp:228] layer1-act does not need backward computation.
I0522 10:19:19.015712 25251 net.cpp:228] layer1-scale does not need backward computation.
I0522 10:19:19.015714 25251 net.cpp:228] layer1-bn does not need backward computation.
I0522 10:19:19.015718 25251 net.cpp:228] layer1-conv does not need backward computation.
I0522 10:19:19.015722 25251 net.cpp:228] input does not need backward computation.
I0522 10:19:19.015725 25251 net.cpp:270] This network produces output layer139-conv
I0522 10:19:19.015731 25251 net.cpp:270] This network produces output layer150-conv
I0522 10:19:19.015736 25251 net.cpp:270] This network produces output layer161-conv
I0522 10:19:19.015911 25251 net.cpp:283] Network initialization done.
unknow layer type yolo 
unknow layer type yolo 
save prototxt to prototxt/yolov4.prototxt
save caffemodel to caffemodel/yolov4.caffemodel

```



# Work on ACL

1.modify yolov4.prototxt

two input nodes

```
input: "data"
input_shape {
  dim: 1
  dim: 3
  dim: 416
  dim: 416
}
input: "img_info"
input_shape {
  dim: 1
  dim: 4
}
```

modify two upsample nodes

```
layer {
    bottom: "layer118-conv"
    top: "layer119-upsample"
    name: "layer119-upsample"
    type: "Upsample"
    upsample_param {
        scale: 1
        stride: 2
    }
}

layer {
    bottom: "layer128-conv"
    top: "layer129-upsample"
    name: "layer129-upsample"
    type: "Upsample"
    upsample_param {
        scale: 1
        stride: 2
    }
}
```

three yolo layer

```
layer {
	bottom: "layer139-conv"
	top: "yolo1_coords"
	top: "yolo1_obj"
	top: "yolo1_classes"
	name: "yolo1"
	type: "Yolo"
	yolo_param {
		boxes: 3
		coords: 4
		classes: 80  
		yolo_version: "V3"
		softmax: true
		background: false
    }
}

layer {
	bottom: "layer150-conv"
	top: "yolo2_coords"
	top: "yolo2_obj"
	top: "yolo2_classes"
	name: "yolo2"
	type: "Yolo"
	yolo_param {
		boxes: 3
		coords: 4
		classes: 80  
		yolo_version: "V3"
		softmax: true
		background: false
	}
}

layer {
	bottom: "layer161-conv"
	top: "yolo3_coords"
	top: "yolo3_obj"
	top: "yolo3_classes"
	name: "yolo3"
	type: "Yolo"
	yolo_param {
		boxes: 3
		coords: 4
		classes: 80  
		yolo_version: "V3"
		softmax: true
		background: false
	}
}
```

one YoloV3DetectionOutput

```
layer {
       name: "detection_out3"
       type: "YoloV3DetectionOutput"
       bottom: "yolo1_coords" 
       bottom: "yolo2_coords"
       bottom: "yolo3_coords"
       bottom: "yolo1_obj"
       bottom: "yolo2_obj" 
       bottom: "yolo3_obj"
       bottom: "yolo1_classes"
       bottom: "yolo2_classes"
       bottom: "yolo3_classes" 
       bottom: "img_info"
       top: "box_out"
       top: "box_out_num"
       yolov3_detection_output_param {
                           boxes: 3
                           classes: 80
                           relative: true
                           obj_threshold: 0.5
                           score_threshold: 0.5
                           iou_threshold: 0.45
                           pre_nms_topn: 512
                           post_nms_topn: 1024
                           biases_high: 142
                           biases_high: 110
                           biases_high: 192
                           biases_high: 243
                           biases_high: 459
                           biases_high: 401
                           biases_mid: 36
                           biases_mid: 75  
                           biases_mid: 76
                           biases_mid: 55
                           biases_mid: 72
                           biases_mid: 146
                           biases_low: 12
                           biases_low: 16
                           biases_low: 19
                           biases_low: 36
                           biases_low: 40
                           biases_low: 28
       }
}
```

ATC 

```
atc --model=./yolov4.prototxt \
    --weight=./yolov4.caffemodel \
    --framework=0 \
    --output=./yolov4 \
    --output_type=FP32 \
    --soc_version=Ascend310 \
    --insert_op_conf=./aipp_yolov3.cfg
```

